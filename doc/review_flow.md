Develop guide (focus on review process)
==================================================

- [gitlab](#gitlab) 
- [upsource](#upsource)

# Gitlab

##### Steps for development
- create feature as a branch "feature--\*" under branch "develop"
- add code and finish the feature
- create merge request (on gitlab) to "develop"
  - choose assignee: yourself and your reviewer. This is important so that this merge request appear in both your view and your reviewer's view.
- wait for it to be reviewed and merged
  - reviewer should comment on code block (on gitlab), this comment will trigger notification to requester via email
  - requester can reply the comment, and edit code to resolve issue. This will also trigger email notification to reviewer
  - once reviewer is happy with the code, he can resolve the comment
- once all comments are resolved, reviewer can approve the change and accept the merge

##### Steps for release (master vs develop)
when develop branch accumulates enough features, we need to move them to master this can be done as follows
- update master branch: ```git checkout master; git pull``` 
- merge master to develop: ```git checkout develop; git pull; git merge master```
- merge develop to master: ```git checkout master; git merge develop```

##### Some useful command line tool 
- view all the branches 
```
$ git branch -a
```

- checkout a branch 
```
$ git checkout BRANCH-NAME
```

- create new feature (branch) 
```
$ git checkout -b feature--NAME
```

- push (and create) branch to origin
```
$ git push --set-upstream origin feature--NAME
```

- update (and prune) merged branches 
```
$ git remote update origin --prune 
```

##### IDE integration
- Jetbuild (idea)
  - follow[gitlab tutorial](https://ken.io/note/gitlab-code-review-tutorial) for IDE (idea) integration
  - can do:
    - create feature branch
    - write code
    - commit
    - push
    - create merge request
    - do simple merge-request review, and leave comment to the whole merge request
  - cannot do
    - cannot review single file or single line of code
    - cannot approve merge-request. We might need to think about this. 

##### To improve the flow
- IDE integration
- Consider to use a different tool than gitlab
- review (with comment) is still painful
- show build status for code review
- show test status for code review

##### Resource
- [gitflow tutorial](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
- [gitlab flow](https://gitlab.com/help/topics/gitlab_flow.md)
- [gitlab tutorial](https://ken.io/note/gitlab-code-review-tutorial)

# Upsource 
- No good, require [payment](https://www.jetbrains.com/upsource/buy/#new)

##### Resources
- [jetbrains upsource](https://www.jetbrains.com/upsource/) 
- [integration with gitlab](https://www.jetbrains.com/help/upsource/synchronizing-a-gitlab-project.html)

##### Demo 
- nothing.