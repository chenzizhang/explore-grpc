explore-grpc
=================================

### Targets
- use protobuf/grpc to generate source code
- rpc call between java & c++
- explore [review flow](doc/review_flow.md)
- explore [testing story](doc/testing.md)

### To build and run examples
0. [Install bazel](https://docs.bazel.build/versions/master/install.html)

1. Build the binary
```
$ bazel build :all
```

2. Start server
```
$ ./bazel-bin/java-app-server
```
or 
``` 
$ ./bazel-bin/cpp-app-server
```

3. Start client
```
$ ./bazel-bin/java-app-client
```
or 
```
$ ./bazel-bin/cpp-app-client
```

### Resource 
- [grpc](https://grpc.io/)
- [Bazel grpc support](https://github.com/rules-proto-grpc)
- Maybe we can also use Gradle, but I haven't verified the part for cpp. [Gradle example](https://stackoverflow.com/questions/55260686/gradle-java-kotlin-c-protobuf-in-the-same-project)

### IDE integration 
- for Java: IntelliJ IDEA 
- for C++: CLion