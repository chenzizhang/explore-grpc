package lib;

import app.Request;
import app.Response;

public class TaskHandler {
    public static Response handle(Request req) {
        return Response.newBuilder().setResult(req.getA() + req.getB()).build();
    }
}
