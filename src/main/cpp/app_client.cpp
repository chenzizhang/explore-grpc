#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h>

#include "src/main/proto/task.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using task::Request;
using task::Response;
using task::Sum;

class SumClient {
 public:
  SumClient(std::shared_ptr<Channel> channel) : stub_(Sum::NewStub(channel)) {}

  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  void Simple() {
    // Data we are sending to the server.
    Request request;
    request.set_a(4);
    request.set_b(2);

    // Container for the data we expect from the server.
    Response response;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->Simple(&context, request, &response);

    // Act upon its status.
    if (status.ok()) {
    	std::cout << "Response received: " << response.result() << std::endl;
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
    }
  }

 private:
  std::unique_ptr<Sum::Stub> stub_;
};

int main(int argc, char** argv) {
  // Instantiate the client. It requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint specified by
  // "target_str".
  std::string target_str;
  target_str = "localhost:50051";

  // We indicate that the channel isn't authenticated (use of
  // InsecureChannelCredentials()).
  SumClient sum(grpc::CreateChannel(target_str, grpc::InsecureChannelCredentials()));
  sum.Simple();

  return 0;
}
