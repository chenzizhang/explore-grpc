package lib;

import org.junit.Test;

import app.Request;
import lib.TaskHandler;

import static org.junit.Assert.*;

public class TaskHandlerTest {
    @Test public void testSomeLibraryMethod() {
        int a = 4, b = 2;
        Request request = Request.newBuilder().setA(a).setB(b).build();
        int result = TaskHandler.handle(request).getResult();
        assertEquals(result, a + b);
    }

    @Test public void simpleTest() {
        int a = 4, b = 2;
        assertEquals(b + a, a + b);
    }
}
